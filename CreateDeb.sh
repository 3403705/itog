#!/bin/bash

mkdir -p /tmp/createdebpack/{DEBIAN,usr}
mkdir -p /tmp/createdebpack/usr/local/bin

docker cp myapp:/app/. /tmp/createdebpack/usr/local/bin/

cat > /tmp/createdebpack/DEBIAN/control  << EOF
Package: mav-deb-pack
Version: 0.1
Maintainer: Minkov A V
Architecture: all
Depends: bash, iputils-ping
Description: My first deb pack ...
EOF


cat > /tmp/createdebpack/DEBIAN/postinst << EOF
#!/bin/bash

# uncomment for debug
# set -eux

echo "========== Postinstall started =========="
case "$1" in
configure)
#    echo "===== preinst install ====="
     echo "===== postinst install ====="
    ;;

abort-upgrade)
    echo "===== postinst abort-upgrade ====="
    ;;

abort-remove)
    echo "===== postinst abort-remove ====="
    ;;

*)
    echo "===== postinst called with unknown argument \'$1' =====" >&2
    exit 1
    ;;
esac

echo "------ Postinstall success ------"
EOF

cat > /tmp/createdebpack/DEBIAN/postrm << EOF
#!/bin/bash

echo "========== Postrm started =========="
case "$1" in
remove)
    echo "===== postrm remove ====="
    ;;

upgrade)
    echo "===== postrm upgrade ====="
    ;;

abort-install)
    echo "===== postrm abort-install ====="
    ;;

failed-upgrade)
    echo "===== postrm failed-upgrade ====="
    exit 1
    ;;

purge)
    echo "===== postrm purge ====="
    ;;

abort-upgrade)
    echo "===== postrm abort-upgrade ====="
    ;;

*)
    echo "===== postrm called with unknown argument \'$1' =====" >&2
    exit 1
    ;;
esac

echo "------ Postrm success ------"

EOF

cat > /tmp/createdebpack/DEBIAN/preinst << EOF
#!/bin/bash

# uncomment for debug
# set -eux

echo "========== Preinstall started =========="
case "$1" in
install)
    echo "===== preinst install ====="
    ;;

upgrade)
    echo "===== preinst upgrade ====="
    exit 1;
    ;;


abort-upgrade)
    echo "===== preinst abort upgrade ====="
    ;;

*)
    echo "===== preinst called with unknown argument '$1' =====" >&2
    ;;
esac

echo "------ Preinstall success ------"

EOF


cat > /tmp/createdebpack/DEBIAN/prerm  << EOF
#!/bin/bash

echo "========== Prerm started =========="
case "$1" in
remove)
    echo "===== prerm remove ====="
#    ololo
    ;;

upgrade)
    echo "===== prerm upgrade ====="
    ;;

failed-upgrade)
    echo "===== prerm failed-upgrade ====="
    ;;

*)
    echo "===== prerm called with unknown argument \'$1' =====" >&2
    exit 1
    ;;
esac

echo "------ Prerm success ------"

EOF


chmod +x /tmp/createdebpack/DEBIAN/{control,postinst,postrm,preinst,prerm}

dpkg -b /tmp/createdebpack mav-deb-pack.deb

